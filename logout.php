<?php 
	// Activate session
	session_start();
	 
	// Delete all sessions
	session_destroy();
	 
	// Redirect page while sending logout message
	header("location:index.php?message=logout");
?>