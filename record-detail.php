<?php session_start();
$con = mysqli_connect('localhost', 'root', '', 'recordDB') or die(mysqli_error());
require 'header.php';
error_reporting(E_ERROR | E_PARSE);

$id_record = $_GET['id_rec'];
$username = $_SESSION['username'];
$readrecord = mysqli_query($con, "SELECT * from saved_record where username='$username' and id_record='$id_record'");
$readrecord_arr = [];

while ($row = mysqli_fetch_assoc($readrecord)) {
	$readrecord_arr = explode(";", $row['record']);
}

$query = mysqli_query($con, "SELECT * FROM dataset");
$var = 0;
$process = array();

// To calculate Euclidean Distance
while ($row = mysqli_fetch_array($query)) {
	$temp = pow($row['Clump_Thickness'] - (int)$readrecord_arr[0], 2) +
		pow($row['Uniformity_of_Cell_Size'] - (int)$readrecord_arr[1], 2) +
		pow($row['Uniformity_of_Cell_Shape'] - (int)$readrecord_arr[2], 2) +
		pow($row['Marginal_Adhesion'] - (int)$readrecord_arr[3], 2) +
		pow($row['Single_Epithelial_Cell_Size'] - (int)$readrecord_arr[4], 2) +
		pow((int)$row['Bare_Nuclei'] - (int)$readrecord_arr[5], 2) +
		pow($row['Bland_Chromatin'] - (int)$readrecord_arr[6], 2) +
		pow($row['Normal_Nucleoli'] - (int)$readrecord_arr[7], 2) +
		pow($row['Mitoses'] - (int)$readrecord_arr[8], 2);

	$process[$var] = array('result' => $temp, 'class' => $row['Class']);

	$var = $var + 1;
}

$res = array();
foreach ($process as $row) {
	$res[] = $row['result'];
}
array_multisort($res, SORT_ASC, $process);
$malignan = 0;
$benign = 0;

// For Binary Classification
for ($i = 0; $i < 5; $i++) {
	if ($process[$i]['class'] == 2) {
		$benign = $benign + 1;
	}
	if ($process[$i]['class'] == 4) {
		$malignan = $malignan + 1;
	} else {
		$malignan = 1;
		$benign = 1;
	}
}

// To generate new result based on dataset classes
$class = 0;
if ($malignan > $benign) {
	$class = 4;
} else {
	$class = 2;
}

$temp0 = $readrecord_arr[0];
$temp1 = $readrecord_arr[1];
$temp2 = $readrecord_arr[2];
$temp3 = $readrecord_arr[3];
$temp4 = $readrecord_arr[4];
$temp5 = $readrecord_arr[5];
$temp6 = $readrecord_arr[6];
$temp7 = $readrecord_arr[7];
$temp8 = $readrecord_arr[8];

// then record the result in the dataset table.
mysqli_query($con, "INSERT INTO dataset VALUES('','','$temp0','$temp1','$temp2','$temp3','$temp4','$temp5','$temp6','$temp7','$temp8','$class')");


$result = mysqli_query($con, "SELECT * FROM dataset WHERE Class = '$class'");
$att1 = '';
$att2 = '';
$att3 = '';
$att4 = '';
$att5 = '';
$att6 = '';
$att7 = '';
$att8 = '';
$att9 = '';

while ($row = mysqli_fetch_array($result)) {
	$att1 = $att1 . ", " . $row['Clump_Thickness'];
	$att2 = $att2 . ", " . $row['Uniformity_of_Cell_Size'];
	$att3 = $att3 . ", " . $row['Uniformity_of_Cell_Shape'];
	$att4 = $att4 . ", " . $row['Marginal_Adhesion'];
	$att5 = $att5 . ", " . $row['Single_Epithelial_Cell_Size'];
	$att6 = $att6 . ", " . $row['Bare_Nuclei'];
	$att7 = $att7 . ", " . $row['Bland_Chromatin'];
	$att8 = $att8 . ", " . $row['Normal_Nucleoli'];
	$att9 = $att9 . ", " . $row['Mitoses'];
}

$att1 = substr($att1, 2);
$att2 = substr($att2, 2);
$att3 = substr($att3, 2);
$att4 = substr($att4, 2);
$att5 = substr($att5, 2);
$att6 = substr($att6, 2);
$att7 = substr($att7, 2);
$att8 = substr($att8, 2);
$att9 = substr($att9, 2);

?>

<script>
	function goBack() {
		window.history.back();
	}
</script>

<div class="x_title">
	<h2>Success! Diagnosis Result</h2>
	<div class="clearfix"></div>
</div>
<?php if (isset($benign)) { ?>
	<button onclick="goBack()" class="alert alert-info alert-dismissible fade in" style="padding: 8px;" role="alert">Go Back</button>
	<input type="hidden" id="malignan" value="<?php echo $malignan; ?>">
	<input type="hidden" id="benign" value="<?php echo $benign; ?>">
	<div class="x_content">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<?php if ($malignan > $benign) {
					echo "<h3><b>Malignant (Kanker Ganas)</b></h3>";
					$temp = $malignan / ($malignan + $benign) * 100;
				} else {
					echo "<h3><b>Benign (Kanker Jinak)</b></h3>";
					$temp = $benign / ($malignan + $benign) * 100;
				} ?>
				<h4 style="padding-top: 10px;"><b>Input Data :</b></h4>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p>Clump Thickness = <?php echo $readrecord_arr[0]; ?></p>
					<p>Uniformity of Cell Size = <?php echo $readrecord_arr[1]; ?></p>
					<p>Uniformity of Cell Shape = <?php echo $readrecord_arr[2]; ?></p>
					<p>Marginal Adhesion = <?php echo $readrecord_arr[3]; ?></p>
					<p>Single Epithelial Cell Size = <?php echo $readrecord_arr[4]; ?></p>
					<p>Bare Nuclei = <?php echo $readrecord_arr[5]; ?></p>
					<p>Bland Chromatin = <?php echo $readrecord_arr[6]; ?></p>
					<p>Normal Nucleoli = <?php echo $readrecord_arr[7]; ?></p>
					<p>Mitoses = <?php echo $readrecord_arr[8]; ?></p>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="x_content">
					<canvas id="pieChart"></canvas>
				</div>
			</div>
			<input type="hidden" id="att1" value="<?php echo $att1; ?>">
			<input type="hidden" id="att2" value="<?php echo $att2; ?>">
			<input type="hidden" id="att3" value="<?php echo $att3; ?>">
			<input type="hidden" id="att4" value="<?php echo $att4; ?>">
			<input type="hidden" id="att5" value="<?php echo $att5; ?>">
			<input type="hidden" id="att6" value="<?php echo $att6; ?>">
			<input type="hidden" id="att7" value="<?php echo $att7; ?>">
			<input type="hidden" id="att8" value="<?php echo $att8; ?>">
			<input type="hidden" id="att9" value="<?php echo $att9; ?>">
			<div class="col-md-12 col-sm-12 col-xs-12 x_content">
				<center>
					<div id="myDiv"></div>
				</center>
			</div>
		</div>
	</div>
<?php }
require 'footer.php'; ?>