<?php session_start();
require 'header.php';

$con = mysqli_connect('localhost', 'root', '', 'recordDB') or die(mysqli_error());

$username = $_SESSION['username'];
if ($_SESSION['UserStatus'] == 0) {
	$readrecord = mysqli_query($con, "SELECT * from saved_record where username='$username'");
} else {
	$readrecord = mysqli_query($con, "SELECT * from saved_record");
}

$check = mysqli_num_rows($readrecord);
?>

<style scoped>
	div.row {
		background: #fafafa;
	}

	a.list-container {
		width: 100%;
		margin: 0;
		padding: 0;
		color: inherit;
	}

	ul.list {
		list-style-type: none;
		width: 100%;
		margin: 0;
		padding: 0;
	}

	ul.list li {
		display: flex;
		position: relative;
		font: bold 45px/1.2 Helvetica, Verdana, sans-serif;
		padding: 10px 20px;
	}

	ul.list li:hover {
		background: #eee;
		cursor: pointer;
		color: #555;
	}

	ul.list li p {
		align-self: center;
		font: 12px/1.2 Helvetica, sans-serif;
		padding-left: 30px;
	}
</style>

<div class="x_title">
	<h2>Diagnosis Result</h2>
	<div class="clearfix"></div>
</div>
<form method="post" action="record-delete.php">
	<button type="submit" name="submit" class="alert alert-danger alert-dismissible fade in" style="padding: 8px;">Delete All</button>
</form>
<div class="x_content">
	<div class="row">
		<?php
		if ($check > 0) {
			$readrecord_arr_id = [];
			$readrecord_arr_name = [];

			while ($row = mysqli_fetch_assoc($readrecord)) {
				$readrecord_arr_id[] =  $row['id_record'];
				$readrecord_arr_name[] = $row['username'];
				$readrecord_arr_pname[] = $row['patient_name'];
			}

			for ($i = 0; $i < $check; $i++) {
				if ($_SESSION['UserStatus'] == 0) {
					echo '
						<a class="list-container" href=record-detail.php?id_rec=' . $readrecord_arr_id[$i] . '>
							<ul class="list">
								<li>
								<span class="list-span">' . $readrecord_arr_id[$i] . '</span>
								<p>' . $readrecord_arr_name[$i] .  '</p>
								<p>' . $readrecord_arr_pname[$i] .  '</p></li>
							</ul> 
						</a>';
				} else {
					echo '
						<a class="list-container">
							<ul class="list">
								<li>
									<h5>Please login first.</h5>
								</li>
							</ul> 
						</a>';
				}
			}
		} else {
			echo '
				<a class="list-container">
					<ul class="list">
						<li>
							<h5>No records.</h5>
						</li>
					</ul> 
				</a>';
		}
		?>
		<!-- <a href="record-detail.php">
			<div class="col-md-4 col-sm-6 col-xs-12" style="background-color: yellow;">
				
			</div>
		</a> -->
	</div>
</div>
<?php require 'footer.php'; ?>