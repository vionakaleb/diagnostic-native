<?php 
  session_start();
  if($_SESSION['status']!="login"){
    header("location:login.php?message=not_login");
  }
?>

<?php include 'header.php'; ?>
<div class="x_title">
    <h2>About</h2>
    <div class="clearfix"></div>
</div>
<div class="x_content">
    <div class="clearfix text-center"></div>
      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
        <div class="well profile_view">
          <div class="col-sm-12">
            <h4 class="brief"><i>Developer</i></h4>
            <div class="left col-xs-7">
              <h2>Viona Z. A. Kaleb</h2>
              <p><strong>Major: </strong>Comp. IT 2014</p>
            </div>
            <div class="right col-xs-5 text-center">
              <img src="images/dev.jpg" alt="" class="img-circle img-responsive">
            </div>
          </div>
          <div class="col-xs-12 bottom text-center" style="margin-top: 5px;">
            <div class="col-xs-12 col-sm-6 emphasis text-left">
              <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                </i><i class="fa fa-comments-o"></i> 
              </button>
            </div>
            <div class="col-xs-12 col-sm-6 emphasis text-right">
              <button type="button" class="btn btn-primary btn-xs">
                <i class="fa fa-facebook-square"> </i> View Profile
              </button>
            </div>
          </div>
        </div>
      </div>

</div>
<?php include 'footer.php'; ?>