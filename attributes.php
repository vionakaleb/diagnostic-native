<?php 
  session_start();
  if($_SESSION['status']!="login"){
    header("location:login.php?message=not_login");
  }
?>

<?php include 'header.php'; ?>
<div class="x_title">
    <h2>Attributes Description</h2>
    <div class="clearfix"></div>
</div>
<div class="x_content">
	<div class="row">
		<h4><b>Clump Thickness</b></h4> 
		<blockquote>
			<p>Benign cells that tend to be grouped in monolayer / one layer, whereas cancer cells are grouped in multilayer.</p>
			<!-- <p>Sel-sel jinak yang cenderung dikelompokkan dalam monolayer/satu lapisan, sedangkan sel-sel kanker dikelompokkan dalam multilayer.</p> -->
		</blockquote> 

		<h4><b>Uniformity of Cell Size</b></h4>
        <blockquote>
        	<p>Cancer cells that tend to vary in size, the more variations in size, the more necessary in determining whether the cell is cancerous or not.</p>
        	<!-- <p>Sel-sel kanker yang cenderung bervariasi dalam ukuran, semakin banyak variasi ukurannya maka semakin perlu dalam menentukan apakah sel tersebut kanker atau tidak.</p> -->
        </blockquote>

		<h4><b>Uniformity of Cell Shape</b></h4> 
        <blockquote>
        	<p>Cancer cells tend to vary in shape, so it is necessary to determine whether the cell is cancerous or not.</p>
        	<!-- <p>Sel-sel kanker yang cenderung bervariasi dalam bentuknya, sehingga perlu dalam menentukan apakah sel tersebut kanker atau tidak.</p> -->
        </blockquote>
		
		<h4><b>Marginal Adhesion</b></h4> 
        <blockquote>
        	<p>Normal cells tend to remain clustered (together). Cancer cells tend to separate. So, losing this form of adhesion signifies cell malignancy.</p>
        	<!-- <p>Sel normal cenderung tetap bergerombol(bersama-sama). Sel kanker cenderung memisah. Jadi, kehilangan bentuk adhesi ini menandakan keganasan sel.</p> -->
        </blockquote>
		
		<h4><b>Single Epithelial Cell size</b></h4> 
        <blockquote>
        	<p>Related to uniformity of cell size. Epithelial cells that are significantly enlarged are likely to become malignant cells that cause cancer.</p>
        	<!-- <p>Berkaitan dengan keseragaman ukuran sel. Sel-sel epitel yang signifikan membesar kemungkinan akan menjadi sel-sel ganas yang mengakibatkan kanker.</p> -->
        </blockquote>
		
		<h4><b>Bare Nuclei</b></h4> 
        <blockquote>
        	<p>It is a term used for cell nuclei that is not surrounded by cytoplasm. Usually in benign tumors.</p>
        	<!-- <p>Merupakan istilah yang digunakan untuk inti sel yang tidak dikelilingi oleh sitoplasma. Biasanya berada pada tumor jinak.</p> -->
        </blockquote>
		
		<h4><b>Bland Chromathin</b></h4> 
        <blockquote>
        	<p>It is a uniform texture of the nucleus or cell nucleus that can be seen in benign cells. In cancer cells, this chromatin tends to be rough.</p>
        	<!-- <p>Merupakan keseragaman tekstur dari nukleus atau inti sel yang dapat dilihat dalam sel jinak. Dalam sel-sel kanker kromatin ini cenderung kasar.</p> -->
        </blockquote>
		
		<h4><b>Normal Nucleoli</b></h4> 
        <blockquote>
        	<p>It is a small structure seen in the cell nucleus. In normal nucleolus cells these are usually very small and not even visible. In nucleolus cancer cells it is more prominent, and sometimes larger than the cell nucleus.</p>
        	<!-- <p>Merupakan struktur kecil yang terlihat dalam inti sel. Dalam sel normal nucleolus ini biasanya sangat kecil bahkan tidak terlihat. Dalam sel-sel kanker nucleolus ini lebih menonjol, dan kadang-kadang lebih besar dari inti sel.</p> -->
        </blockquote>
		
		<h4><b>Mitoses</b></h4> 
        <blockquote>
        	<p>It is the process of dividing a single cell so that it becomes two identical cells. Normal cell conditions for mitosis are very low. In a state of cancer the cell will do mitosis very high.</p>
        	<!-- <p>Merupakan proses pembelahan sel tunggal sehingga menjadi dua sel yang identik. Keadaan normal sel melakukan mitosis sangat rendah. Dalam keadaan kanker sel akan melakukan mitosis dengan sangat tinggi.</p> -->
        </blockquote>
	</div>
</div>
<style>
	p{
		font-size: 13px;
	}
</style>
<?php include 'footer.php'; ?>