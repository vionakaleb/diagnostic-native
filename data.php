<?php
	$con = mysqli_connect('localhost', 'root', '','recordDB') or die(mysqli_error());
	$var = 0;
	if (isset($_POST['submit'])) {
		# code...
	//10,2,5,2,7,4,8,3,6
		$query = mysqli_query($con, "SELECT * FROM dataset");
		$dataBaru  = array($_POST['a1'],$_POST['a2'],$_POST['a3'],$_POST['a4'],$_POST['a5'],$_POST['a6'],$_POST['a7'],$_POST['a8'],$_POST['a9']);
		$process = array();
		while ($row = mysqli_fetch_array($query)) {
			$temp = pow($row['Clump_Thickness'] - $dataBaru[0],2) + 
					pow($row['Uniformity_of_Cell_Size'] - $dataBaru[1],2) + pow($row['Uniformity_of_Cell_Shape'] - $dataBaru[2],2) + 
					pow($row['Marginal_Adhesion'] - $dataBaru[3],2) + pow($row['Single_Epithelial_Cell_Size'] - $dataBaru[4],2) + 
					pow($row['Bare_Nuclei'] - $dataBaru[5],2) + pow($row['Bland_Chromatin'] - $dataBaru[6],2) + 
					pow($row['Normal_Nucleoli'] - $dataBaru[7],2) + pow($row['Mitoses'] - $dataBaru[8],2);
			
			$process[$var] = array('result' => $temp, 'class' => $row['Class']);

		 	$var = $var + 1; 
		}
		$res = array();
		foreach ($process as $row) {
			$res[] = $row['result'];
		}
		array_multisort($res, SORT_ASC, $process);
		$malignan = 0;
		$benign = 0;
		for ($i=0; $i < 5; $i++) { 
			if ($process[$i]['class']==2) {
				$benign = $benign + 1;
			}
			if ($process[$i]['class']==4) {
				$malignan = $malignan + 1;
			}
		}
	}

?>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="//www.chartjs.org/assets/Chart.min.js"></script>
</head>
<body>

<h2>Breast Cancer Diagnostic - System</h2>
<!-- <p id="demo"></p>
<p id="wkwk"></p>
 -->

<form method="post" action="data.php">
	<label>Clump_Thickness</label>
	<input type="number" name="a1">
	<label>Uniformity_of_Cell_Size</label>
	<input type="number" name="a2">
	<label>Uniformity_of_Cell_Shape</label>
	<input type="number" name="a3"> <br><br>
	<label>Marginal_Adhesion</label>
	<input type="number" name="a4">
	<label>Single_Epithelial_Cell_Size</label>
	<input type="number" name="a5">
	<label>Bare_Nuclei</label>
	<input type="number" name="a6"> <br><br>
	<label>Bland_Chromatin</label>
	<input type="number" name="a7">
	<label>Normal_Nucleoli</label>
	<input type="number" name="a8">
	<label>Mitoses</label>
	<input type="number" name="a9"><br><br>
	<input type="submit" name="submit" value="masukin">
</form>
<canvas id="updating-chart" width="500" height="300"></canvas>
<?php
if ($var!=0) {
	echo "Clump_Thickness = ".$dataBaru[0]."<br>";
	echo "Uniformity_of_Cell_Size = ".$dataBaru[1]."<br>";
	echo "Uniformity_of_Cell_Shape = ".$dataBaru[2]."<br>";
	echo "Marginal_Adhesion = ".$dataBaru[3]."<br>";
	echo "Single_Epithelial_Cell_Size = ".$dataBaru[4]."<br>";
	echo "Bare_Nuclei = ".$dataBaru[5]."<br>";
	echo "Bland_Chromatin = ".$dataBaru[6]."<br>";
	echo "Normal_Nucleoli = ".$dataBaru[7]."<br>";
	echo "Mitoses = ".$dataBaru[8]."<br>";
	echo "<br><br><b>Output</b><br>";
	echo "\nMalignant = ".$malignan.", Benign = ".$benign; echo "<br><br>";
	if ($malignan>$benign) {
		echo "<b>MALIGNANT (Kanker Ganas)</b>";
	}
	else
		echo "<b>BENIGN (Kanker Jinak)</b>";	
}	
?>

</body>
</html>

<script>

allData = [[7,8],[9,4]];

$(document).ready(function(){ 

    $.ajax({async : false});
    $.getJSON("sessionResult.php", function(result){
     
        $.each(result, function(i, field){
           allData[i] = [field.id,field.id];
           // console.log(allData);                            
         });
        //
   });
});

var canvas = document.getElementById('updating-chart'),
    ctx = canvas.getContext('2d'),
    startingData = {
      labels: [1, 2, 3, 4, 5, 6, 7],
      datasets: [
          {
              fillColor: "rgba(220,220,220,0.2)",
              strokeColor: "rgba(220,220,220,1)",
              pointColor: "rgba(220,220,220,1)",
              pointStrokeColor: "#fff",
              data: [65, 59, 80, 81, 56, 55, 40]
          },
          {
              fillColor: "rgba(151,187,205,0.2)",
              strokeColor: "rgba(151,187,205,1)",
              pointColor: "rgba(151,187,205,1)",
              pointStrokeColor: "#fff",
              data: [28, 48, 40, 19, 86, 27, 90]
          }
      ]
    },
    latestLabel = startingData.labels[6];

// Reduce the animation steps for demo clarity.
var myLiveChart = new Chart(ctx).Line(startingData, {animationSteps: 15});


setInterval(function(){
  // Add two random numbers for each dataset
  myLiveChart.addData(allData[latestLabel], ++latestLabel);
  // Remove the first point so we dont just add values forever
  myLiveChart.removeData();
}, 5000);





$(document).ready(function(){
    $("button").click(function(){
        // $.get("sessionResult.php", function(data, status){
        //     alert("Data: " + data + "\nStatus: " + status);
        // });
   		$.getJSON("sessionResult.php", function(result){
            $.each(result, function(i, field){
              alert("Data: " + field.id);  
            });
        });
    });
    
obj = JSON.parse(text2);
document.getElementById("demo").innerHTML =
obj[0].CodeNumber;
var count =0;


for(var i = 0; i < 698; i++) {
    // var obj = obj.data[i];
     count = count+parseInt(obj[i].ClumpThickness);
    // console.log(obj.CodeNumber);

};
document.getElementById("wkwk").innerHTML = count;



</script>
