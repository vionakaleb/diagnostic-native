<?php
  if(isset($_GET['message'])){
    if($_GET['message'] == "failed"){
      echo "Login failed!! Wrong username and password!";
    }else if($_GET['message'] == "logout"){
      echo "Anda telah berhasil logout";
    }else if($_GET['message'] == "not_login"){
      echo "You have to login first!!";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Breast Cancer Diagnostic - System</title>
    <link rel="icon" type="image/png" sizes="32x32" href="images/logo.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/logo.png">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
          <script src="../assets/js/ie8-responsive-file-warning.js"></script>
          <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <!-- Login -->
        <div id="login" class="animate form">
          <section class="login_content">
            <form action="loginProcess.php" method="post">
              <h1>User Login</h1>
              <div>
                <input type="text" name="username" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default" type="submit" name="login">Log in</button>
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>
              <div class="clearfix"></div>
              <div class="separator">

                <p class="change_link">Already a user?
                  <a href="#toregister" class="to_register"> Login as user here. </a>
                </p>
                <div class="clearfix"></div>
                <br />
                <div>
                  <h2><i class="fa fa-desktop" style="font-size: 26px;"></i> Breast Cancer Diagnostic - System</h2>
                  <p>©2019 All Rights Reserved. Breast Cancer Diagnostic is a decision making system. Privacy and Terms.</p>
                </div>
              </div>
            </form>
            <!-- form -->
          </section>
          <!-- content -->
        </div>
        <!-- User Login (Register) -->
        <div id="register" class="animate form">
          <section class="login_content">
            <form action="loginProcess.php" method="post">
              <h1>User Login</h1>
              <div>
                <input type="text" name="username" class="form-control" placeholder="Username" required="" />
              </div>
              <!-- <div>
                <input type="email" class="form-control" placeholder="Email" required="" />
              </div> -->
              <div>
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default" type="submit" name="login">Log in</button>
              </div>
              
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Are you admin ?
                  <a href="#tologin" class="to_register"> Log in as admin here. </a>
                </p>
                <div class="clearfix"></div>
                <br>
                <div>
                  <h2><i class="fa fa-desktop" style="font-size: 26px;"></i> Breast Cancer Diagnostic - System</h2>
                  <p>©2019 All Rights Reserved. Breast Cancer Diagnostic is a decision making system. Privacy and Terms.</p>
                </div>
              </div>
            </form>
            <!-- form -->
          </section>
          <!-- content -->
        </div>
      </div>
    </div>
  </body>
</html>
