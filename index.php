<?php 
  session_start();
  if($_SESSION['status']!="login"){
    header("location:login.php?message=not_login");
  }
?>

<?php require 'header.php' ?>
	<div class="splash fade-in">
		<h1 class="splash-title fade-in">Breast Cancer Diagnostic - System</h1>
		<h1 class="splash-title fade-in">Viona Z. A. Kaleb</h1>
        <a href="diagnose.php" class="splash-arrow fade-in"><img src="splash/img/down-arrow.png" alt=""/></a>
	</div>
<?php require 'footer.php' ?>